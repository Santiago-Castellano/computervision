import numpy as np
import cv2
 
 
def main():
   
    camaraWeb()
 
def camaraWeb():
   
    cap = cv2.VideoCapture(0)
    fondo = None
    while(True):
        # Captura frame por frame
        ret,frame = cap.read()
   
        gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    	# Aplicamos suavizado para eliminar ruido
        gris = cv2.GaussianBlur(gris, (21, 21), 0)

        if fondo is None:
            fondo = gris
            continue

        ## Calculo de la diferencia entre el fondo y el frame actual
        resta = cv2.absdiff(fondo, gris)
#
        ## Aplicamos un umbral
        umbral = cv2.threshold(resta, 25, 255, cv2.THRESH_BINARY)[1]
#
        ## Dilatamos el umbral para tapar agujeros
        umbral = cv2.dilate(umbral, None, iterations=2)
        contornosimg = umbral.copy()

        # obtener los contornos
        contours,_ = cv2.findContours(contornosimg, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for c in contours:
		# Eliminamos los contornos más pequeños
            if cv2.contourArea(c) < 4000:
                continue

		    # Obtenemos el bounds del contorno, el rectángulo mayor que engloba al contorno
            (x, y, w, h) = cv2.boundingRect(c)
		    # Dibujamos el rectángulo del bounds
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    	# Mostramos las imágenes de la cámara, el umbral y la resta
        cv2.imshow("Camara", frame)
        #cv2.imshow("Umbral", umbral)
        #cv2.imshow("Resta", resta)
        #cv2.imshow("Contorno", contornosimg)

       
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
   
    cap.release()
    cv2.destroyAllWindows()
 
 
 
if __name__ == '__main__':
    main()